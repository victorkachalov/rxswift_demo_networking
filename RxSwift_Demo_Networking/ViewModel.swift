//
//  ViewModel.swift
//  RxSwift_Demo_Networking
//
//  Created by Victor Kachalov on 03/03/2018.
//  Copyright © 2018 Victor Kachalov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct ViewModel {
    
    var searchText = Variable<String?>("")

    let APIProvider: APIProvider
    var data: Driver<[Repository]>
    
    init(APIProvider: APIProvider) {
        self.APIProvider = APIProvider
        
        data = self.searchText.asObservable()
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged({ (first, second) -> Bool in
               return first == second
            })
            .flatMapLatest {
                APIProvider.getRepositories($0!)
            }.asDriver(onErrorJustReturn: [])
        
    }
    
}
