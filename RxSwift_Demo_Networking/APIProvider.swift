//
//  APIProvider.swift
//  RxSwift_Demo_Networking
//
//  Created by Victor Kachalov on 03/03/2018.
//  Copyright © 2018 Victor Kachalov. All rights reserved.
//

import Foundation
import RxSwift

class APIProvider {
    
    func getRepositories(_ gitHubId: String) -> Observable<[Repository]> {
        guard !gitHubId.isEmpty, let url = URL(string: "https://api.github.com/users/\(gitHubId)/repos") else {
            return Observable.just([])
        }
        
        return URLSession.shared
        .rx.json(request: URLRequest(url: url))
        .retry(3)
            .map {
                var repositories = [Repository]()
                if let items = $0 as? [[String : AnyObject]] {
                    items.forEach {
                        guard let name = $0["name"] as? String,
                            let url = $0["html_url"] as? String
                            else { return }
                        repositories.append(Repository(name: name, url: url))
                    }
                }
                return repositories
            }
    }
}
