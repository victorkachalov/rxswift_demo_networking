//
//  Repository.swift
//  RxSwift_Demo_Networking
//
//  Created by Victor Kachalov on 03/03/2018.
//  Copyright © 2018 Victor Kachalov. All rights reserved.
//

import Foundation

struct Repository {
    let name: String
    let url: String
}
