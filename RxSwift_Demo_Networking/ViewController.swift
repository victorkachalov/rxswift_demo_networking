//
//  ViewController.swift
//  RxSwift_Demo_Networking
//
//  Created by Victor Kachalov on 03/03/2018.
//  Copyright © 2018 Victor Kachalov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    var searchBar: UISearchBar {
        return searchController.searchBar
    }
    
    var repositoriesViewModel: ViewModel?
    let api = APIProvider()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureSearchController()
        
        repositoriesViewModel = ViewModel(APIProvider: api)
        if let viewModel = repositoriesViewModel {
            viewModel.data.drive(tableView.rx.items(cellIdentifier: "cell")) {_, repository, cell in
                cell.textLabel?.text = repository.name
                cell.detailTextLabel?.text = repository.url
                }.disposed(by: disposeBag)
            searchBar.rx.text.bind(to: viewModel.searchText).disposed(by: disposeBag)
            searchBar.rx.cancelButtonClicked.map{""}.bind(to: viewModel.searchText).disposed(by: disposeBag)
            
            viewModel.data.asDriver()
                .map {
                    "\($0.count) Repositories"
                }
                .drive(navigationItem.rx.title)
                .disposed(by: disposeBag)
        }
        
    }
    
    func configureSearchController() {
        searchController.obscuresBackgroundDuringPresentation = false
        searchBar.showsCancelButton = true
        searchBar.text = "victorkachalov"
        searchBar.placeholder = "Enter user"
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
    }
    
}
