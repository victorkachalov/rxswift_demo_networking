//
//  RxSwift_Demo_NetworkingTests.swift
//  RxSwift_Demo_NetworkingTests
//
//  Created by Victor Kachalov on 03/03/2018.
//  Copyright © 2018 Victor Kachalov. All rights reserved.
//

import XCTest
@testable import RxSwift_Demo_Networking

class RxSwift_Demo_NetworkingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
